package com.sda.service;

import java.sql.Date;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sda.dao.UpcomingServiceDaoImpl;
import com.sda.model.UpcomingService;

/**
 * The @Service annotation marks that fact that this class should be initialized as
 * a Spring bean when it scans this package for beans
 * 
 * @author bogdan
 *
 */
@Service
@Transactional
public class UpcomingServiceServiceImpl {

    /**
     * Because of the @Autowired annotation Spring will inject here a bean of type LocationDao
     */
    @Autowired
    private UpcomingServiceDaoImpl upcomingServiceDaoImpl;

    public List<UpcomingService> getAllUpcomingServices() {
        System.out.println("  Entering UpcomingServiceService.getAllUpcomingServices");

        System.out.println("  Calling UpcomingServiceDao.getAllUpcomingServicess");
        List<UpcomingService> upcomingServices = upcomingServiceDaoImpl.getAllUpcomingServices();

        System.out.println("  Exiting UpcomingServiceService.getAllUpcomingServices");
        return upcomingServices;
    }

    public void saveUpcomingService(Date serviceDate) {
        System.out.println("  Entering UpcomingServiceService.saveUpcomingService");

        UpcomingService upcomingService = new UpcomingService();

        System.out.println("  Calling UpcomingServiceDao.saveUpcomingService for upcomingServiceDate: " + serviceDate);
        upcomingServiceDaoImpl.saveUpcomingService(upcomingService);

        System.out.println("  Exiting UpcomingServiceService.saveUpcomingService");
    }

    public void deleteUpcomingServiceById(int upcomingServiceId) {
        System.out.println("  Entering LocationService.deleteLocation");

        // Getting the location object from the database based on its id
        UpcomingService upcomingService = upcomingServiceDaoImpl.getUpcomingServiceById(upcomingServiceId);

        System.out.println("  Calling UpcomingServiceDao.deleteUpcomingService for upcomingServiceId: " + upcomingServiceId);
        upcomingServiceDaoImpl.deleteUpcomingService(upcomingService);

        System.out.println("  Exiting LocationService.deleteLocation");
    }

    public UpcomingService getUpcomingServiceById(int upcomingServiceId) {
        System.out.println("  Entering UpcomingServiceService.getUpcomingServiceById");

        System.out.println("  Calling UpcomingServiceDao.getUpcomingServiceById for upcomingServiceId: " + upcomingServiceId);
        UpcomingService upcomingService = upcomingServiceDaoImpl.getUpcomingServiceById(upcomingServiceId);

        System.out.println("  Exiting UpcomingServiceService.getUpcomingServiceById");

        return upcomingService;
    }

    public void updateUpcomingService(int upcomingServiceId, Date serviceDate, String serviceType, String comments) {
        System.out.println("  Entering UpcomingServiceService.updateUpcomingService");

        System.out.println("  Calling UpcomingServiceDao.getUpcomingServiceById for upcomingSeviceId: " + upcomingServiceId);
        UpcomingService upcomingService = upcomingServiceDaoImpl.getUpcomingServiceById(upcomingServiceId);

    

        System.out.println("  Calling UpcomingServiceDao.updateUpcomingService for serviceDate: " + serviceDate);
        upcomingServiceDaoImpl.updateUpcomingService(upcomingService);

        System.out.println("  Exiting UpcomingServiceService.updateUpcomingService");
    }

	
}
