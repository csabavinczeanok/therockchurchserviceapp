package com.sda.controller;

import java.sql.Date;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sda.model.UpcomingService;
import com.sda.service.UpcomingServiceServiceImpl;


/**
 * The controller class acts as a servlet intercepting requests from the client
 * 
 * The location where it intercepts them is: http://localhost:8080/sdaSpringMvcProject/location/....
 * built by: serverAddress/projectName/controllerRequestMapping/methodRequestMapping
 * 
 * @author bogdan
 *
 */
@Controller
@RequestMapping("/upcomingService")
public class UpcomingServiceController {

    /**
     * Because of the @Autowired annotation Spring will inject here a bean of type LocationService
     */
    @Autowired
    private UpcomingServiceServiceImpl upcomingServiceServiceImpl;
    
//    @Autowired
//    private CountryService countryService;

    /**
     * Method will listen to requests matching it's request mapping
     * http://localhost:8080/sdaSpringMvcProject/location/list
     * 
     * @param model - is the object used to transfer information from the controller to the JSP
     * @return
     */
    @RequestMapping("/list")
    public String listUpcomingService(ModelMap model) {
        System.out.println("Entering UpcomingServiceController.listUpcomingService");
        
        // retrieving the list of all locations from the database
        List<UpcomingService> upcomingServices = upcomingServiceServiceImpl.getAllUpcomingServices();
//        List<Country> countries = countryService.getAllCountries();

        // adding the list of locations to the model so it will be available in the jsp
        // locationList is the name of the attribute that will be iterated in the jsp file
        model.addAttribute("upcomingServiceList", upcomingServices);
//        model.addAttribute("countryList", countries);
        

        System.out.println("Exiting UpcomingServiceController.listUpcomingService"); 
        
        System.out.println(upcomingServices.size());
        for (UpcomingService upcomingService: upcomingServices) {
        System.out.println(upcomingService.getServiceDate());
        }
        // this is the name of the jsp file that will be used to display the page
        return "upcomingServiceListView";
    }
    

    /** 
     * Method will listen only to POST requests matching it's request mapping
     * http://localhost:8080/sdaSpringMvcProject/location/delete
     * 
     * @param model - is the object used to transfer information from the controller to the JSP
     * @param locationId - is a parameter that we expect will come in the request from the client
     * @return
     */
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public String deleteUpcomingService(ModelMap model, @RequestParam("serviceId") int serviceId) {
        System.out.println("Entering UpcomingServiceController.deleteUpcomingService");
        
        // deleting the location from database based on its id
        upcomingServiceServiceImpl.deleteUpcomingServiceById(serviceId);
        
        // retrieving the list of all locations from the database
        // this list will no longer have the deleted location
        List<UpcomingService> upcomingService = upcomingServiceServiceImpl.getAllUpcomingServices();

        // adding the list of locations to the model so it will be available in the jsp
        // locationList is the name of the attribute that will be iterated in the jsp file
        model.addAttribute("upcomingServiceList", upcomingService);

        System.out.println("Exiting UpcomingServiceController.deleteUpcomingService");

        // this is the name of the jsp file that will be used to display the page
        return "upcomingServiceListView";
    }

    /**
     * This method expects that the request will come with a parameter containing the 
     * location name for the new location that we want to save in the database
     * 
     * @param model
     * @param newLocationName
     * @return
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String createUpcomingService(ModelMap model, @RequestParam("serviceDate") Date newServiceDate) {
        System.out.println("Entering UpcomingServiceController.createUpcomingService");

        // Saving the new location in the database
        upcomingServiceServiceImpl.saveUpcomingService(newServiceDate);

        // retrieving the list of all locations from the database
        // this list will contain the newly added location as well
        List<UpcomingService> upcomingServices = upcomingServiceServiceImpl.getAllUpcomingServices();

        // adding the list of locations to the model so it will be available in the jsp
        // locationList is the name of the attribute that will be iterated in the jsp file
        model.addAttribute("upcomingServiceList", upcomingServices);
        
        System.out.println("Exiting UpcomingServiceController.createUpcomingService");

        // this is the name of the jsp file that will be used to display the page
        return "upcomingServiceListView";
    }

   
    
    /**
     * When updating a location there are two steps:
     *  1. Select the location that we want to update and go to a screen that will allow us to update it
     *  2. Save the updated name in the database
     *  
     * This method is the first step that based on the id of the location we want to update takes us
     * to the page that allows us to update it
     * @param model
     * @param locationToBeUpdatedId
     * @return
     */
    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public String updateUpcomingServiceView(ModelMap model, @RequestParam("serviceId") int upcomingServiceToBeUpdatedId) {
        System.out.println("Entering UpcomingServiceController.updateUpcomingServiceView");
        
        // Retrieving the location that we want to update from the database
        UpcomingService upcomingServiceToBeUpdated =upcomingServiceServiceImpl.getUpcomingServiceById(upcomingServiceToBeUpdatedId);

        // Adding the location object to the model so we can display its name in the browser
        model.addAttribute("upcomingService", upcomingServiceToBeUpdated);

        System.out.println("Exiting UpcomingServiceController.updateUpcomingServiceView");

        // Showing the page that allows us to update it
        return "upcomingServiceUpdateView";
    }

    /**
     * This is the second step. We now have the id(which does not change) of the location that we want to update
     * and the new name for the location. We then need to update the entry in the database and return to the
     * page that shows all locations.
     * 
     * @param model
     * @param locationId
     * @param locationName
     * @return
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String updateUpcomingServiceSave(ModelMap model, @RequestParam("serviceId") int serviceId, 
    		@RequestParam("serviceDate") Date serviceDate, @RequestParam("serviceType") String servicetype, 
    		@RequestParam("comments") String comments) {
        System.out.println("Entering UpcomingServiceController.updateUpcomingServiceSave");

        // Updating the database entry with the new name
        upcomingServiceServiceImpl.updateUpcomingService(serviceId, serviceDate, servicetype, comments);

        // retrieving the list of all locations from the database
        // this list will contain the updated location as well
        List<UpcomingService> upcomingServices = upcomingServiceServiceImpl.getAllUpcomingServices();

        // adding the list of locations to the model so it will be available in the jsp
        // locationList is the name of the attribute that will be iterated in the jsp file
        model.addAttribute("upcomingServiceList", upcomingServices);
        
        System.out.println("Exiting UpcomingServiceController.updateUpcomingServiceSave");

        // this is the name of the jsp file that will be used to display the page
        return "upcomingServiceListView";
    }

}
