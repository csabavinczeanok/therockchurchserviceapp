package com.sda.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sda.model.UpcomingService;

/**
 * The @Repository annotation marks that fact that this class should be initialized as
 * a Spring bean when it scans this package for beans
 * 
 * @author bogdan
 *
 */
@Repository
public class UpcomingServiceDaoImpl {

    @Autowired
    private SessionFactory sessionFactory;

    public List<UpcomingService> getAllUpcomingServices() {
        System.out.println("    Entering UpcomingServiceDao.getAllUpcomingServices");

        Query query = sessionFactory.getCurrentSession().createQuery("from UpcomingServices", UpcomingService.class);
        List<UpcomingService> upcomingServices = query.getResultList();

        System.out.println("    Exiting UpcomingServiceDao.getAllUpcomingServices");
        return upcomingServices;
    }
    

    public void saveUpcomingService(UpcomingService upcomingService) {
        System.out.println("    Entering UpcomingServiceDao.saveUpcomingService");

        sessionFactory.getCurrentSession().save(upcomingService);

        System.out.println("    Exiting UpcomingServiceDao.saveUpcomingService");
    }

    public void deleteUpcomingService(UpcomingService upcomingService) {
        System.out.println("    Entering UpcomingServiceDao.deleteUpcomingService");

        sessionFactory.getCurrentSession().delete(upcomingService);

        System.out.println("    Exiting UpcomingServiceDao.deleteUpcomingService");
    }

    public UpcomingService getUpcomingServiceById(int serviceId) {
        System.out.println("    Entering UpcomingServiceDao.getUpcomingServiceById");

        UpcomingService upcomingService = sessionFactory.getCurrentSession().get(UpcomingService.class, serviceId);

        System.out.println("    Exiting UpcomingServiceDao.getUpcomingServiceById");

        return upcomingService;
    }

    public void updateUpcomingService(UpcomingService upcomingService) {
        System.out.println("    Entering UpcomingServiceDao.updateUpcomingService");

        sessionFactory.getCurrentSession().saveOrUpdate(upcomingService);

        System.out.println("    Exiting UpcomingServiceDao.updateUpcomingService");
    }

}
