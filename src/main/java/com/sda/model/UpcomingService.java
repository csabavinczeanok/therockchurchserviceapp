package com.sda.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "upcoming_services")
public class UpcomingService {
	
	@Id
	@Column(name = "id")
	private String serviceId;
	@Column(name = "service_date")
	private String serviceDate;
	@Column(name = "service_location")
	private String serviceLocation;
	@Column(name = "service_type")
	private String serviceType;
	@Column(name = "comments")
	private String comments;

	public String getServiceId() {
		return serviceId;
	}
	
	public String getServiceDate() {
		return serviceDate;
	}

	public void setServiceDate(String serviceDate) {
		this.serviceDate = serviceDate;
	}

	public String getServiceLocation() {
		return serviceLocation;
	}

	public void setServiceLocation(String serviceLocation) {
		this.serviceLocation = serviceLocation;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;

	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
}
