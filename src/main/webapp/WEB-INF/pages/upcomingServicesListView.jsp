<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<body>
    <jsp:include page="menu.jsp"></jsp:include>
    
    <h2>View all Upcoming Services</h2>
	<table border="1">
	   <tr>
	       <td>Service Id</td>
	       <td>Service Date</td>
	       <td>Service Location</td>
	       <td>Service Type</td>
           <td>Comments</td>
	   </tr>
		<c:forEach items="${upcomingServiceList}" var="upcomingService">
			<tr>
				
				<td>${upcomingService.getServiceId()}</td>
				<td>${upcomingService.getServiceDate() }</td>
                <td>${upcomingService.getServiceLocation() }</td>
                <td>${upcomingService.getServiceType() }</td>
                <td>${upcomingService.getComments() }</td>
				<td>
					<form method="post"
						action="${pageContext.request.contextPath}/upcomingService/delete">
						<input type="hidden" name="serviceId"
							value="${upcomingService.getServiceId()}"> 
						<input type="submit" value="Delete">
					</form>
				</td>

				<td>
					<form method="get"
						action="${pageContext.request.contextPath}/upcomingService/update">
						<input type="hidden" name="serviceId"
							value="${upcomingService.getServiceId()}"> 
						<input type="submit" value="Update">
					</form>
				</td>
			</tr>
		</c:forEach>
	</table>
	
	<br/><br/>
	
	<h2>Add new Upcoming Service</h2>
	
	<form method="post"
        action="${pageContext.request.contextPath}/upcomingService/create">
        Service Date: <input type="text" name="serviceDate" > <br /> 
        Service Location: <input type="text" name="serviceLocation" > <br />
        Service Type: <input type="text" name="serviceType" > <br /> 
        Comments: <input type="text" name="comments" > <br /> 
       
        <input type="submit" value="Save">
    </form>
</body>
</html>
