package com.sda.service;

import java.sql.Date;
import java.util.List;

import com.sda.model.UpcomingService;

public interface UpcomingServiceService {
    
    List<UpcomingService> getAllUpcomingServices();

    void saveUpcomingService(Date serviceDate);

    void deleteUpcomingServiceById(int upcomingServiceId);

    UpcomingService getUpcomingServiceById(int upcomingServiceId);

    void updateUpcomingService(int upcomingServiceId, Date serviceDate, String serviceType, String comments);
}
