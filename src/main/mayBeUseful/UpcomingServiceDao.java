package com.sda.dao;

import java.util.List;

import com.sda.model.UpcomingService;

public interface UpcomingServiceDao {
    
    List<UpcomingService> getAllUpcomingServices();

    void saveUpcomingService(UpcomingService upcomingService);

    void deleteUpcomingService(UpcomingService upcomingService);

    UpcomingService getUpcomingServiceById(int upcomingServiceId);

    void updateUpcomingService(UpcomingService upcomingService);
}
